+++
title = "Samba In Linux"
date = "2021-05-03"
page_template = "page.html"
+++

How to set up sambashare in linux to share your files form windows, linux, mac or mobile devices to each other using within a same network.
<!-- more -->

## What is Sambashare?
Samba is the standard Windows interoperability suite of programs for Linux and Unix.
It is an open-source software suite that runs on Unix/Linux based platforms but is able to communicate with Windows clients like a native application which is able to provide the services like File & print services, Authentication and Authorization, Name resolution, and Service announcement (browsing) by employing the Common Internet File System (CIFS).

## How to set up sambashare in linux?

### Installation
Install samba in your linux machine according to the distribution you use.

For example: In debian based distributions: `$ sudo apt-get install samba`<br>
In arch based distributions: `$ sudo pacman -Sy samba`

### Configuration
Then take a look at `/etc/samba/smb.conf` file.
It may or may not exist according to the distribution you use.<br>
If it exists, take a backup using `$ sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.bak`.<br>
Then create `smb.conf` in `/etc/samba/` and open with your favourite text editor (for example i use vim here) with sudo access: `$ sudo vim /etc/samba/smb.conf` and add the following lines.

`[name of the folder to share]`<br>
`comment = File sharing using sambashare`<br>
`path = full path to the folder to share`<br>
`browseable =yes`<br>
`read only = yes/no`<br>

#### For Example:
To share the files located in `/home/useless/sharefolder/`, configure the following:

`[FileSharing]`<br>
`comment = File sharing using sambashare`<br>
`path = /home/useless/sharefolder/`<br>
`browseable = yes`<br>
`read only = no`<br>
Save the file and then follow the steps below:

Enable and start samba service in your system:
`$ sudo systemctl enable smb && sudo systemctl start smb`

Add your current (Regular) user to samba and assign password to access the files:
`$ sudo smbpasswd -a <username>`

For Example: If my regular linux username is `useless`, `$ sudo smbpasswd -a useless` and set the password.

If you use any firewall, allow samba to send and receive files through the firewall.<br>
For Example: In ufw firewall, `$ sudo ufw allow cifs` or/and `$ sudo ufw allow smb`

You're all set now. Find your ip using `$ ip addr` or `$ ifconfig` and access the files from any device within same network.

To access the files in windows system,
`Go to Start --> run --> type \\ip-address --> Ok`

<img src="/assets/images/windowsrunip.jpg" alt="access sambashare from windows using ip address" style="width:383px;max-width:100%;">

Then Enter your username and password you set earlier and access it in file manager.

<img src="/assets/images/sambainfilemanager.jpg" alt="accessing sharefolder using sambashare in windows file manager" style="width:383px;max-width:100%;">

To access the files in linux system open a file manager that has gvfs support, search `smb://(ip-address)` in the search bar of file manager, enter the username and password and access the files.
