+++
title = "anime terminal"
date = "2022-05-27"
page_template = "page.html"
+++

Watch your favourite anime from linux terminal; you don't need a browser to watch anime now.
<!-- more -->

### So what is anime terminal?
[anime-terminal](https://github.com/whoisYoges/anime-terminal) is a fork of [ani-cli](https://github.com/pystardust/ani-cli). ani-cli is a cool project and i used to use it. But later on, many stuff were added into it which were unnecessary for me. So, I forked it and renamed it to anime-terminal.

Now, ani-cli is a vaste project with a lot of features and contributors. But I didn't needed all those features and was happy with old ani-cli with my some changes to it. And it seems there are more people like me who didn't need all those vaste changes, so people are using anime-terminal and it's getting some popularity.

## How does anime-terminal work?
anime-terminal is a bash script that scrapes the website gogoanime and provies the result according to the user input.
You can use it to watch/stream and download anime from command line. It has got a lot of advantages like you don't need to open a browser so that how much a potato your computer is, you can easily watch anime.
Also, we don't need to deal with all those crappy ads which are available in the browser.

So, if you want to give it a try, checkout [anime-terminal](https://github.com/whoisYoges/anime-terminal) in github.