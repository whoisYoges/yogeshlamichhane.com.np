+++
title = "Barahi - my first gtk theme"
date = "2020-11-01"
page_template = "page.html"
+++

It was quiet a some time, I started using linux and was curious about linux systems, that I wanted to create my own theme. I was much inspired to hacker-like (black-green) themes at that time and thus made my first linux gtk theme. It was horrible but, really learned a lot of things.
<!-- more -->

## How did I create a gtk theme?
I was totally clueless when i started. I couldn't find a good documentation or a tutorial to make a theme.
And I found about a project called [oomox](https://github.com/themix-project/oomox), what it does a combines your all color, icon packs, and settings and generates a theme based on it. I just selected black and green colors and generated a theme and named it **Barahi**.

I myself used the theme and told some of my friend to try it. They rated it average but I was quiet happy and satisfied cause it was the first theme I made it myself. And after some time, I uploaded it to [pling](https://www.pling.com/p/1440685/) and quiet surprisingly it got few hundred of downloads.

Again, after almost a year, I again had a urge to make the theme better and modified it. Most of the things were made better but one main thing got worse which I didn't knew for months. So I used to use a windows manager (DWM) in my linux system and there was no application launcher like in gnome, kde or xfce. And few months later I had to install xfce for some reason and then only; I found out that the color scheme in application launcher looked absolutely horrible. But until the time, I had no urge to again continue or make a linux theme. So it's still left horrible and is still available there in [pling](https://www.pling.com/p/1440685/).