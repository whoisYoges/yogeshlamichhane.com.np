+++
title = "How to reach me?"
page_template = "page.html"
+++

<p class="quote">
	❝ Introvert people are actually talkative when they get comfortable with someone. ❞
</p>

### So, Let's have a chat.

Leave a message at any of the following:
- instagram: <a href="https://instagram.com/whoisYoges" target="_blank" rel="noopener noreferrer">@whoisYoges</a> 
-  telegram: <a href="https://t.me/whoisYoges" target="_blank" rel="noopener noreferrer">@whoisYoges</a> 
-  twitter: <a href="https://twitter.com/whoisYoges" target="_blank" rel="noopener noreferrer">@whoisYoges</a> 
-  email: <a href="mailto:hello@yogeshlamichhane.com.np" target="_blank" rel="noopener noreferrer">hello@yogeshlamichhane.com.np</a> 

There's a good chance that you'll be replied sooner in twitter and telegram.

### How to not be annoying?

- Don't ask issues you are getting in your system.
- If you don't want conversation just use email.
- Don't call me, just text or email me. I prefer text and mails rather than calls until I get comfortable.